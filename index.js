var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 3000;
server.listen(port, function () {
    console.log('Server listening at port %d', port);
});
//app.use(express.static(__dirname + '/public'));   // Routing
//app.listen(8080);
io.set('transports', ['websocket',
    'flashsocket',
    'htmlfile',
    'xhr-polling',
    'jsonp-polling',
    'polling']);

app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    next();
});

var numUsers = 0;
var users = [];
var messages = [];
var storeusers = function (name) {
    users.push({name: name});
};

io.on('connection', function (socket) {
    var addedUser = false;
    socket.on('add user', function (username) {
        numUsers++;
        socket.emit('countuser', {numUsers: numUsers});
        if (addedUser) {
            return;
        }
        storeusers(username);
        socket.username = username;
        addedUser = true;
        socket.broadcast.emit("add chatter", socket.username);
        users.forEach(function (message) {
            socket.emit("add chatter", message.name);
        });
        messages.forEach(function (message) {
            socket.emit('new message', {
                username: message.username,
                message: message.message,
                date: message.date
            });
        });

    });
    function ConvData(data) {
        if (data < 10) {
            data = "0" + data;
        }
        return data;
    }
    function GetDate() {
        var d = new Date();
        var fulldate = ConvData(d.getDate()) + "." + ConvData(d.getMonth()) + "." + d.getFullYear() + " (" + ConvData(d.getHours()) + ":" + ConvData(d.getMinutes()) + ":" + ConvData(d.getSeconds()) + ")";
        return(fulldate);
    }
    socket.on('new message', function (data) {
        var fulldate = GetDate();
        var onemessage = {username: socket.username,
            message: data,
            date: fulldate};
        messages.push(onemessage);
        socket.emit('new message', {
            username: socket.username,
            message: data,
            date: fulldate
        });
        socket.broadcast.emit('new message', {
            username: socket.username,
            message: data,
            date: fulldate
        });
    });
    socket.on('disconnect', function (name) {
        socket.broadcast.emit('remove chatter', socket.username);
        numUsers--;
        for (var i = users.length - 1; i >= 0; i--) {
            if (users[i].name == socket.username) {
                users.splice(i, 1);
            }
        }
    });
});
//app.listen(process.env.PORT);
//console.log(process.env.PORT);